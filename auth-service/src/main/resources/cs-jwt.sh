keytool -genkeypair -alias cs-jwt -validity 3650 -keyalg RSA -dname "CN=jwt,OU=jwt,O=jwt,L=Bei Jing,S=Bei Jing,C=CN" -keypass 123456 -keystore cs-jwt.jks -storepass 123456 -storetype PKCS12
keytool -list -rfc --keystore cs-jwt.jks | openssl x509 -inform pem -pubkey
keytool -list -rfc --keystore cs-jwt.jks | openssl x509 -inform pem -pubkey -noout