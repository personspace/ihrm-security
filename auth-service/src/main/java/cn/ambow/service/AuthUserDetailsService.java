package cn.ambow.service;

import cn.ambow.client.UserClient;
import cn.ambow.pojo.po.User;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * 授权用户信息服务实现类
 *
 * @author shanglei
 */
@Slf4j
public class AuthUserDetailsService implements UserDetailsService {

    private final static String ROLE_PREFIX = "ROLE_";

    @Resource
    private UserClient userClient;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userClient.getUser(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("用户不存在 - %s", username));
        }

        List<GrantedAuthority> authorities = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(user.getPermissions())) {
            authorities = user.getPermissions().stream()
                    .map(permission -> new SimpleGrantedAuthority(ROLE_PREFIX + permission.getPermissionCode()))
                    .collect(toList());
        }

        return new org.springframework.security.core.userdetails.User(username, user.getPassword(), authorities);
    }
}
