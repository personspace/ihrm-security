package cn.ambow.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * OAuth2认证服务配置.
 *
 * @author shanglei
 */
@Configuration
@EnableAuthorizationServer
public class OAuth2ServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private Environment env;

    @Resource(name = "authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Resource
    private UserDetailsService userDetailsService;

    @Resource
    private PasswordEncoder passwordEncoder;


    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.allowFormAuthenticationForClients()
                .checkTokenAccess("permitAll()");
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        String clientID = env.getProperty("security.oauth2.client.client-id");
        String secret = env.getProperty("security.oauth2.client.client-secret");
        String[] scope = Objects.requireNonNull(env.getProperty("security.oauth2.client.scope")).split(",");
        String[] grantType = Objects.requireNonNull(env.getProperty("security.oauth2.client.grant-type")).split(",");
        int accessTokenValiditySeconds = Integer.parseInt(Objects.requireNonNull(env.getProperty("security.oauth2.client.access-token-validity-seconds")));
        int refreshTokenValiditySeconds = Integer.parseInt(Objects.requireNonNull(env.getProperty("security.oauth2.client.refresh-token-validity-seconds")));

        clients.inMemory().withClient(clientID)
                .secret(passwordEncoder.encode(secret))
                .scopes(scope)
                .authorizedGrantTypes(grantType)
                .accessTokenValiditySeconds(accessTokenValiditySeconds)
                .refreshTokenValiditySeconds(refreshTokenValiditySeconds);
    }

    /**
     * 授权服务端点配置
     *
     * @param endpoints
     * @throws Exception
     */
    @Override
    public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.tokenStore(tokenStore())
                .reuseRefreshTokens(false)
                .userDetailsService(userDetailsService)
                .tokenEnhancer(jwtTokenEnhancer())
                .authenticationManager(authenticationManager);
    }


    @Bean
    TokenStore tokenStore() {
        return new JwtTokenStore(jwtTokenEnhancer());
    }

    @Bean
    JwtAccessTokenConverter jwtTokenEnhancer() {
        String alias = env.getRequiredProperty("jwt.alias");
        String password = env.getRequiredProperty("jwt.password");
        String secretKeyPath = env.getRequiredProperty("jwt.secret-key");
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource(secretKeyPath), password.toCharArray());
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair(alias));
        return converter;
    }
}
