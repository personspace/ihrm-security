package cn.ambow.client;

import cn.ambow.pojo.po.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * 用户客户端
 *
 * @author shanglei
 * @date 2020/12/21
 */
@Slf4j
@Component
public class UserClient {

    private final static String GET_USER = "http://127.0.0.1:8001/internal/user/{username}";

    @Resource
    private RestTemplate restTemplate;

    public User getUser(String username) {
        try {
            return restTemplate.getForObject(GET_USER, User.class, username);
        } catch (Exception e) {
            log.error("getUser error - {},{}", ExceptionUtils.getStackTrace(e), username);
        }
        return null;
    }
}
