package cn.ambow.enumeration;

/**
 * 授权类型.
 *
 * @author Peter
 */
public enum GrantType {
    PASSWORD("password"),
    REFRESH_TOKEN("refresh_token");

    private final String value;

    GrantType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
