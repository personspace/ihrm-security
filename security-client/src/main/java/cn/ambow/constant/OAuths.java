package cn.ambow.constant;

/**
 * 认证常量.
 *
 * @author Peter
 */
public class OAuths {
    public final static String CLIENT_ID = "client_id";
    public final static String CLIENT_SECRET = "client_secret";
    public final static String USERNAME = "username";
    public final static String PASSWORD = "password";
    public final static String TOKEN = "token";
    public final static String REFRESH_TOKEN = "refresh_token";
    public final static String GRANT_TYPE = "grant_type";
}
