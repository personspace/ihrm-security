package cn.ambow.constant;

/**
 * Redis键值
 *
 * @author Peter
 */
public class RedisKeys {
    public final static String ACCESS_TOKEN_KEY_FORMAT = "access_token_%s"; // 访问令牌键格式
    public final static String REFRESH_TOKEN_KEY_FORMAT = "refresh_token_%s"; // 刷新令牌键格式
    public final static String USER_TOKEN_KEY_FORMAT = "user_token_%s"; // 用户令牌键格式
    public final static String CAPTCHA_KEY_FORMAT = "captcha_%s_%s_%s"; // 验证码键格式
    public final static String LOGIN_USER_FORMAT = "login_user_%s_%s_%s"; // 登录用户名格式
    public final static String LOGIN_PASS_FORMAT = "login_pass_%s_%s_%s"; // 登录密码格式
}
