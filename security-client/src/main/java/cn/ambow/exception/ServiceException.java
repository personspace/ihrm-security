package cn.ambow.exception;

/**
 * 服务级异常
 *
 * @author shanglei
 * @date 2020/11/28
 */
public class ServiceException extends RuntimeException{
    private final int code;
    private final String message;

    public ServiceException() {
        this.code = 88888;
        this.message = "服务异常";
    }

    public ServiceException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
