package cn.ambow.exception;

/**
 * 系统级异常
 *
 * @author shanglei
 * @date 2020/11/28
 */
public class SystemException extends RuntimeException {

    private final int code;
    private final String message;

    SystemException() {
        this.code = 99999;
        this.message = "系统内部错误，请联系管理员";
    }

    public SystemException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


}
