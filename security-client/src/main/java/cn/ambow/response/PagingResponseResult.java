package cn.ambow.response;

import lombok.Data;

/**
 * 分页接口响应
 *
 * @author shanglei
 */
@Data
public class PagingResponseResult {
    private boolean success;
    private int errorCode;
    private String errorMessage;
    private Object payload;
    private int page;
    private int totalPage;
    private int count;
}
