package cn.ambow.response;

import lombok.Data;

/**
 * 接口对象响应
 *
 * @author Peter
 */
@Data
public class ObjectResponseResult<T> {
    private boolean success;
    private int errorCode;
    private String errorMessage;
    private T payload;
}
