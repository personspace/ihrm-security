package cn.ambow.response;

import java.math.BigDecimal;

import static java.math.BigDecimal.ROUND_CEILING;

/**
 * 分页计算工具类
 *
 * @author shanglei
 */
public class Paging {
    /**
     * 计算总页数
     *
     * @param req
     * @param res
     * @return
     */
    public static int totalPage(Pagination req, PagingResponseResult res) {
        int pageSize = req.getPageSize();
        int count = res.getCount();
        return pageSize != 0 ? BigDecimal.valueOf(count).divide(BigDecimal.valueOf(pageSize), ROUND_CEILING).intValue() : 0;
    }
}
