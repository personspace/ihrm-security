package cn.ambow.response;

import org.springframework.http.HttpStatus;

/**
 * http响应返回模型
 *
 * @author shanglei
 */
public class HttpRes {
    /**
     * success
     *
     * @return
     */
    public static ResponseResult success() {
        return success(null);
    }

    /**
     * success
     *
     * @param payload
     * @return
     */
    public static ResponseResult success(Object payload) {
        ResponseResult response = new ResponseResult();
        response.setSuccess(true);
        response.setErrorCode(HttpStatus.OK.value());
        response.setErrorMessage(HttpStatus.OK.getReasonPhrase());
        response.setPayload(payload);
        return response;
    }

    /**
     * error
     *
     * @return
     */
    public static ResponseResult error() {
        return error(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }

    /**
     * error
     *
     * @param errorMessage
     * @return
     */
    public static ResponseResult error(String errorMessage) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR.value(), errorMessage);
    }

    /**
     * 认证错误
     *
     * @param errorMessage
     * @return
     */
    public static ResponseResult authError(String errorMessage) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR.value(), errorMessage);
    }

    /**
     * error
     *
     * @param errorCode
     * @param errorMessage
     * @return
     */
    public static ResponseResult error(int errorCode, String errorMessage) {
        ResponseResult response = new ResponseResult();
        response.setSuccess(false);
        response.setErrorCode(errorCode);
        response.setErrorMessage(errorMessage);
        return response;
    }

}
