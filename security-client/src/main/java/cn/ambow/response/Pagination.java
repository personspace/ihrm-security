package cn.ambow.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * 计算偏移量模型
 *
 * @author shanglei
 */
@Data
public class Pagination {
    private int page = 1;
    private int pageSize = 10;

    /**
     * 计算偏移量
     *
     * @return
     */
    @JsonIgnore
    public int getOffset() {
        return (this.page - 1) * this.pageSize;
    }
}
