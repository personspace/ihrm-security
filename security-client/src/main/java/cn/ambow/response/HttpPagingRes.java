package cn.ambow.response;

import org.springframework.http.HttpStatus;

/**
 * 分页接口模型
 *
 * @author shanglei
 */
public class HttpPagingRes {
    /**
     * ok
     *
     * @param payload
     * @return
     */
    public static PagingResponseResult ok(Object payload, int count) {
        PagingResponseResult response = new PagingResponseResult();
        response.setSuccess(true);
        response.setErrorCode(HttpStatus.OK.value());
        response.setErrorMessage(HttpStatus.OK.getReasonPhrase());
        response.setPayload(payload);
        response.setCount(count);
        return response;
    }

    /**
     * error
     *
     * @return
     */
    public static PagingResponseResult error() {
        return error(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }

    /**
     * error
     *
     * @param errorMessage
     * @return
     */
    public static PagingResponseResult error(String errorMessage) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR.value(), errorMessage);
    }

    /**
     * error
     *
     * @param errorCode
     * @param errorMessage
     * @return
     */
    public static PagingResponseResult error(int errorCode, String errorMessage) {
        PagingResponseResult response = new PagingResponseResult();
        response.setSuccess(false);
        response.setErrorCode(errorCode);
        response.setErrorMessage(errorMessage);
        return response;
    }
}
