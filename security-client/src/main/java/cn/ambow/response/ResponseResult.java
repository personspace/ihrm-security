package cn.ambow.response;

import lombok.Data;

/**
 * 接口返回体
 *
 * @author shanglei
 */
@Data
public class ResponseResult {
    private boolean success;
    private int errorCode;
    private String errorMessage;
    private Object payload;
}
