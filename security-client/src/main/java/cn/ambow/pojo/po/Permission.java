package cn.ambow.pojo.po;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 权限模型
 *
 * @author shanglei
 * @date 2020/12/7
 */
@Data
public class Permission {
    private Long permissionID;
    private String permissionName;
    private String permissionCode;
    private String creator;
    private LocalDateTime createTime;
}
