package cn.ambow.pojo.po;

import lombok.Data;

/**
 * 角色权限模型
 *
 * @author shanglei
 * @date 2020/12/7
 */
@Data
public class RolePermission {
    private Long roleID;
    private Long permissionID;
}
