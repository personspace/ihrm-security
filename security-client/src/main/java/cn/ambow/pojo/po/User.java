package cn.ambow.pojo.po;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户模型
 */
@Data
public class User {
    private Long userID;
    private String username;
    private String nickname;
    private String password;
    private LocalDateTime modifyTime;
    private String creator;
    private LocalDateTime createTime;
    private List<Role> roles;
    private List<Permission> permissions;
}
