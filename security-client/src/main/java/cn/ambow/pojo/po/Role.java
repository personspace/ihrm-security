package cn.ambow.pojo.po;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 角色模型
 *
 * @author shanglei
 * @date 2020/12/7
 */
@Data
public class Role {
    private Long roleID;
    private String roleName;
    private String roleCode;
    private String creator;
    private LocalDateTime createTime;
}
