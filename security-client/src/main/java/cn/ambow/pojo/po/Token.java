package cn.ambow.pojo.po;

import lombok.Data;
import org.springframework.cglib.core.Local;

import java.time.LocalDateTime;

/**
 * token模型
 *
 * @author shanglei
 * @date 2020/12/7
 */
@Data
public class Token {
    private String id;
    private String user;
    private String accessTokenID;
    private String accessToken;
    private String refreshTokenID;
    private String refreshToken;
    private LocalDateTime expireTime;
    private LocalDateTime modifyTime;
    private LocalDateTime createTime;
}
