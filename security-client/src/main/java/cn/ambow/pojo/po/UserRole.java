package cn.ambow.pojo.po;

import lombok.Data;

/**
 * 用户角色模型
 *
 * @author shanglei
 * @date 2020/12/7
 */
@Data
public class UserRole {
    private Long userID;
    private Long roleID;
}
