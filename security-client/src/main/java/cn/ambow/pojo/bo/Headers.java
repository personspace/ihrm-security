package cn.ambow.pojo.bo;

/**
 * Headers
 *
 * @author Peter
 */
public class Headers {
    public final static String CLIENT = "X-Request-Client";
    public final static String AUTHORIZATION = "Authorization";
}
