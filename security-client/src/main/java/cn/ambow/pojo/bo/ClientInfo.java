package cn.ambow.pojo.bo;

import lombok.Data;

/**
 * 客户端信息
 *
 * @author Peter
 */
@Data
public class ClientInfo {
    private String clientID;
    private String clientSecret;
    private String clientType;

    public ClientInfo() {
        this.clientID = "test";
        this.clientSecret = "test";
        this.clientType = "admin";
    }
}
