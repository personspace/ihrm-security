package cn.ambow.pojo.dto;

import lombok.Data;

/**
 * 登录请求体
 *
 * @author shanglei
 * @date 2020/11/30
 */
@Data
public class UserLoginDto {
    private String username;
    private String password;
}
