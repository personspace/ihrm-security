package cn.ambow.pojo.dto;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author shanglei
 * @date 2020/11/29
 */
@Data
public class CreateUserDto {
    private String username;
    private String nickname;
    private String password;
}
