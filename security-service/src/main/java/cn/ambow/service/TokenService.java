package cn.ambow.service;

import cn.ambow.client.AuthClient;
import cn.ambow.exception.ServiceException;
import cn.ambow.exception.SystemException;
import cn.ambow.pojo.JwtToken;
import cn.ambow.pojo.dto.TokenReq;
import cn.ambow.pojo.po.Token;
import cn.ambow.repository.TokenRepository;
import cn.ambow.response.HttpRes;
import cn.ambow.response.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 令牌服务
 *
 * @author shanglei
 * @date 2020/12/24
 */
@Service
@Slf4j
public class TokenService {

    @Resource
    private AuthClient authClient;

    @Resource
    private TokenRepository tokenRepository;

    /**
     * 校验token
     *
     * @param req
     * @return
     */
    public ResponseResult checkToken(TokenReq req) {
        try {
            String token = req.getToken();
            if (StringUtils.isBlank(token)) {
                throw new ServiceException(80001, "token不能为空");
            }
            Token t = tokenRepository.selectByAccessTokenID(token);
            if (t == null) {
                throw new ServiceException(80001, "access_token不存在");
            }
            Map<String, Object> result = authClient.checkToken(t.getAccessToken());
            return HttpRes.success(result);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            log.error("checkToken error - {}, {}", ExceptionUtils.getStackTrace(e), req);
            throw new SystemException(90001, "校验token出错");
        }
    }


    /**
     * 刷新token
     *
     * @param req
     * @return
     */
    public ResponseResult refreshToken(TokenReq req) {
        try {
            String token = req.getToken();
            if (StringUtils.isBlank(token)) {
                throw new ServiceException(80001, "token不能为空");
            }
            Token t = tokenRepository.selectByRefreshTokenID(token);
            if (t == null) {
                throw new ServiceException(80001, "access_token不存在");
            }
            OAuth2AccessToken oAuth2AccessToken = authClient.refreshToken(t.getRefreshToken());
            JwtToken jwtToken = new JwtToken();
            jwtToken.setAccessToken(oAuth2AccessToken.getValue());
            jwtToken.setTokenType(oAuth2AccessToken.getTokenType());
            jwtToken.setRefreshToken(oAuth2AccessToken.getRefreshToken().getValue());
            jwtToken.setExpiresIn(oAuth2AccessToken.getExpiresIn());
            return HttpRes.success(jwtToken);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            log.error("refreshToken error - {}, {}", ExceptionUtils.getStackTrace(e), req);
            throw new SystemException(90001, "刷新token出错");
        }
    }
}
