package cn.ambow.service;


import cn.ambow.client.AuthClient;
import cn.ambow.exception.ServiceException;
import cn.ambow.exception.SystemException;
import cn.ambow.pojo.JwtToken;
import cn.ambow.pojo.bo.ClientInfo;
import cn.ambow.pojo.dto.CreateUserDto;
import cn.ambow.pojo.dto.UserLoginDto;
import cn.ambow.pojo.po.Permission;
import cn.ambow.pojo.po.User;
import cn.ambow.repository.PermissionRepository;
import cn.ambow.repository.UserRepository;
import cn.ambow.response.HttpRes;
import cn.ambow.response.ResponseResult;
import cn.ambow.tokenhandler.AccessTokenService;
import cn.ambow.tokenhandler.TraditionalTokenService;
import cn.ambow.util.Client;
import cn.ambow.util.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author shanglei
 * @date 2020/12/1
 */
@Service
@Slf4j
public class UserService {

    @Resource
    private UserRepository userRepository;

    @Resource
    private PasswordEncoder passwordEncoder;


    @Resource
    private TraditionalTokenService traditionalTokenService;

    @Resource
    private PermissionRepository permissionRepository;

    @Resource
    private AuthClient authClient;

    @Autowired
    @Qualifier("DBAccessTokenServiceImpl")
    private AccessTokenService accessTokenService;

    /**
     * 登录
     *
     * @param loginDto
     * @return
     */
    public ResponseResult login(UserLoginDto loginDto) {
        try {
            String username = loginDto.getUsername();
            String password = loginDto.getPassword();
            if (StringUtils.isBlank(username)) {
                throw new ServiceException(80001, "用户名不能为空");
            }
            if (StringUtils.isBlank(password)) {
                throw new ServiceException(80001, "密码不能为空");
            }
            User user = userRepository.selectByName(username);
            if (user == null) {
                throw new ServiceException(80001, "用户不存在");
            }
            if (!passwordEncoder.matches(password, user.getPassword())) {
                throw new ServiceException(80001, "密码错误");
            }
//            JwtToken jwtToken = tokenService.saveToken(loginUser);
            JwtToken jwtToken = oauthToken(username, password);
            return HttpRes.success(jwtToken);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            log.error("login error - {}, {}", ExceptionUtils.getStackTrace(e), loginDto);
            throw new SystemException(90001, "登录出错");
        }
    }

    /**
     * 授权
     *
     * @param username
     * @param password
     */
    private JwtToken oauthToken(String username, String password) {
        OAuth2AccessToken token = authClient.oauthToken(username, password);
        if (token == null) {
            throw new ServiceException(20000, "用户授权出错");
        }
        JwtToken jwtToken = accessTokenService.addToken(Client.info(), username, token);
        return jwtToken;
    }

    /**
     * 退出
     *
     * @param request
     * @return
     */
    public ResponseResult logout(HttpServletRequest request) {
        try {
            ClientInfo info = new ClientInfo();
            String username = SecurityUtil.getUsername();
            accessTokenService.deleteToken(info, username);
            return HttpRes.success("退出成功");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            log.error("login error - {}, {}", ExceptionUtils.getStackTrace(e), request);
            throw new SystemException(90001, "退出出错");
        }
    }


    public ResponseResult getUser(Long userID) {
        try {
            User user = userRepository.select(userID);
            return HttpRes.success(user);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            log.error("login error - {}, {}", ExceptionUtils.getStackTrace(e), userID);
            throw new SystemException(90001, "获取用户信息出错");
        }
    }


    @Transactional
    public ResponseResult createUser(CreateUserDto userDto) {
        try {
            String username = SecurityUtil.getUsername();
            String encodePassword = passwordEncoder.encode(userDto.getPassword());
            User user = new User();
            user.setUsername(userDto.getUsername());
            user.setNickname(userDto.getNickname());
            user.setPassword(encodePassword);
            user.setCreator(username);
            int i = userRepository.insert(user);
            if (i > 0) {
                return HttpRes.success("创建成功");
            }
            throw new ServiceException(80001, "创建失败");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            log.error("createUser error - {}, {}", ExceptionUtils.getStackTrace(e), userDto);
            throw new SystemException(90002, "创建用户出错");
        }
    }

    public User getUserByUsername(String username) {
        User user = userRepository.selectByName(username);
        if (user != null) {
            List<Permission> permissions = permissionRepository.selectListByUserID(user.getUserID());
            user.setPermissions(permissions);
            return user;
        }
        return null;
    }
}
