package cn.ambow.repository;

import cn.ambow.pojo.po.Token;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * token存储库
 *
 * @author shanglei
 * @date 2020/12/7
 */
@Mapper
public interface TokenRepository {
    int insert(Token token);

    int update(Token token);

    Token select(@Param("id") String id);

    int delete(@Param("id") String id);

    Token selectByAccessTokenID(String accessTokenID);

    Token selectByRefreshTokenID(String refreshTokenID);
}
