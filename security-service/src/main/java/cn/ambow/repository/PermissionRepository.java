package cn.ambow.repository;

import cn.ambow.pojo.po.Permission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 权限存储库
 *
 * @author shanglei
 * @date 2020/12/7
 */
@Mapper
public interface PermissionRepository {
    List<Permission> selectListByUserID(Long userID);
}
