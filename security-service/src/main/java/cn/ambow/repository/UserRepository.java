package cn.ambow.repository;

import cn.ambow.pojo.po.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserRepository {

    User select(@Param("userID") Long userID);

    int insert(User user);

    User selectByName(@Param("username") String username);
}
