//package cn.ambow.config;
//
//import cn.ambow.filter.TokenAuthFilter;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//import javax.annotation.Resource;
//
///**
// * security配置类,
// * 如果走controller请求登录，注释的代码部分不生效，没意义
// *
// * @author shanglei
// * @date 2020/12/1
// */
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//
////    @Autowired
////    @Qualifier("userDetailServiceImpl")
////    private UserDetailsService userDetailsService;
//
////    @Resource
////    private AuthenticationSuccessHandler authenticationSuccessHandler;
////
////    @Resource
////    private AuthenticationFailureHandler authenticationFailureHandler;
////
////    @Resource
////    private AuthenticationEntryPoint authenticationEntryPoint;
////
////    @Resource
////    private LogoutSuccessHandler logoutSuccessHandler;
//
//    @Resource
//    private TokenAuthFilter tokenAuthFilter;
//
//    /**
//     * 设置认证方式
//     *
//     * @param auth
//     * @throws Exception
//     */
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
////        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable();
//        http.authorizeRequests().antMatchers("/user/login", "/user/logout").permitAll()
//                .anyRequest().authenticated();
////        http.formLogin().loginProcessingUrl("/login")
////                .successHandler(authenticationSuccessHandler)
////                .failureHandler(authenticationFailureHandler).and()
////                .exceptionHandling().
////                authenticationEntryPoint(authenticationEntryPoint);
//
////        http.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);
//
//        //每次执行UsernamePasswordAuthenticationFilter之前都会走自定义的token认证
//        http.addFilterBefore(tokenAuthFilter, UsernamePasswordAuthenticationFilter.class);
//    }
//
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//}
