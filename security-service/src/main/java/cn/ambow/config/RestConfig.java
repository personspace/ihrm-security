package cn.ambow.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * json时间序列化、反序列化配置类
 *
 * @author shanglei
 * @date 2020/11/29
 */
@Configuration
public class RestConfig {

    private static final String DATE_TIME_FORMATTER = "yyyy-MM-dd HH:mm:ss";

    private static final String DATE_FORMATTER = "yyyy-MM-dd";

    private static final String TIME_FORMATTER = "HH:mm:ss";

    @Bean
    ObjectMapper objectMapper() {
        return new Jackson2ObjectMapperBuilder()
                .findModulesViaServiceLoader(true)
                .serializerByType(LocalDateTime.class, new LocalDateTimeSerializer(
                        DateTimeFormatter.ofPattern(DATE_TIME_FORMATTER)))
                .deserializerByType(LocalDateTime.class, new LocalDateTimeDeserializer(
                        DateTimeFormatter.ofPattern(DATE_TIME_FORMATTER)))
                .serializerByType(LocalTime.class, new LocalTimeSerializer(
                        DateTimeFormatter.ofPattern(TIME_FORMATTER)))
                .serializerByType(LocalDate.class, new LocalDateSerializer(
                        DateTimeFormatter.ofPattern(DATE_FORMATTER)))
                .build();
    }

    @Bean
    public MappingJackson2HttpMessageConverter mappingjackson2httpmessageconverter() {
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setObjectMapper(objectMapper());
        return mappingJackson2HttpMessageConverter;
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
