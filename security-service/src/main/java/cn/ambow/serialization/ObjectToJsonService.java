package cn.ambow.serialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * json转化
 *
 * @author shanglei
 * @date 2020/11/23
 */
@Component
@Slf4j
public class ObjectToJsonService {

    @Resource
    private ObjectMapper objectMapper;

    /**
     * 对象转json
     *
     * @param object
     * @return
     */
    public String toJson(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("对象[{}]转化时发生异常-----{}", object, e.getStackTrace());
        }
        return null;
    }


    /**
     * json串转对象
     *
     * @param content
     * @param typeReference
     * @param <T>
     * @return
     */
    public <T> T toObject(String content, TypeReference<T> typeReference) {
        try {
            return objectMapper.readValue(content, typeReference);
        } catch (JsonProcessingException e) {
            log.error("对象 [{}] 转化为[{}] 时发生异常-----{}", content, typeReference, e.getStackTrace());
        }
        return null;
    }

    /**
     * json串转对象
     *
     * @param content
     * @param valueType
     * @param <T>
     * @return
     */
    public <T> T toObject(String content, Class<T> valueType) {
        try {
            return objectMapper.readValue(content, valueType);
        } catch (IOException e) {
            log.error("对象 [{}] 转化为[{}] 时发生异常-----{}", content, valueType, e.getStackTrace());
        }
        return null;
    }
}
