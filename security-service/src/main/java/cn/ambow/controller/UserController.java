package cn.ambow.controller;

import cn.ambow.pojo.dto.CreateUserDto;
import cn.ambow.pojo.dto.UserLoginDto;
import cn.ambow.pojo.po.User;
import cn.ambow.response.ResponseResult;
import cn.ambow.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author shanglei
 * @date 2020/12/1
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Resource
    private UserService userService;

    @PostMapping("login")
    public ResponseResult login(@RequestBody UserLoginDto loginDto) {
        return userService.login(loginDto);
    }

    @GetMapping("logout")
    public ResponseResult logout(HttpServletRequest request) {
        return userService.logout(request);
    }

    @PreAuthorize("hasRole('USER_QUERY')")
    @GetMapping("{userID}")
    public ResponseResult getUser(@PathVariable Long userID) {
        return userService.getUser(userID);
    }

    @PreAuthorize("hasRole('USER_ADD')")
    @PostMapping("add")
    public ResponseResult create(@RequestBody CreateUserDto userDto) {
        return userService.createUser(userDto);
    }

}
