package cn.ambow.controller;

import cn.ambow.pojo.dto.CreateUserDto;
import cn.ambow.pojo.dto.UserLoginDto;
import cn.ambow.pojo.po.User;
import cn.ambow.response.ResponseResult;
import cn.ambow.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author shanglei
 * @date 2020/12/1
 */
@RestController
@RequestMapping("/internal/user")
public class InternalUserController {

    @Resource
    private UserService userService;

    @GetMapping("{username}")
    public User getUserByUsername(@PathVariable String username){
        return userService.getUserByUsername(username);
    }
}
