package cn.ambow.controller;

import cn.ambow.pojo.dto.TokenReq;
import cn.ambow.response.ResponseResult;
import cn.ambow.service.TokenService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author shanglei
 * @date 2020/12/1
 */
@RestController
@RequestMapping("token")
public class TokenController {

    @Resource
    private TokenService tokenService;

    @PostMapping("check")
    public ResponseResult checkToken(@RequestBody TokenReq req) {
        return tokenService.checkToken(req);
    }

    @PostMapping("refresh")
    public ResponseResult refreshToken(@RequestBody TokenReq req) {
        return tokenService.refreshToken(req);
    }


}
