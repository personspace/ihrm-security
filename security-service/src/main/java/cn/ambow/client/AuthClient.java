package cn.ambow.client;


import cn.ambow.enumeration.GrantType;
import cn.ambow.pojo.bo.ClientInfo;
import cn.ambow.util.Client;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Map;

import static cn.ambow.constant.OAuths.*;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;


/**
 * 认证服务客户端.
 *
 * @author shanglei
 */
@Slf4j
@Component
public class AuthClient {

    @Resource
    private RestTemplate restTemplate;

    private static final String AUTH_SERVICE = "http://127.0.0.1:10001";

    /**
     * oauthToken
     *
     * @param username
     * @param password
     * @return
     */
    public OAuth2AccessToken oauthToken(String username, String password) {
        try {
            ClientInfo clientInfo = Client.info();
            String clientID = clientInfo.getClientID();
            String clientSecret = clientInfo.getClientSecret();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add(CLIENT_ID, clientID);
            params.add(CLIENT_SECRET, clientSecret);
            params.add(USERNAME, username);
            params.add(PASSWORD, password);
            params.add(GRANT_TYPE, GrantType.PASSWORD.getValue());

            HttpEntity httpEntity = new HttpEntity(params, headers);

            ResponseEntity<OAuth2AccessToken> responseEntity = restTemplate.exchange(AUTH_SERVICE + "/oauth/token", POST, httpEntity, OAuth2AccessToken.class);
            return responseEntity.getBody();
        } catch (Exception e) {
            log.error("oauthToken error - {}, {}, {}", ExceptionUtils.getStackTrace(e), username, password);
        }
        return null;
    }

    /**
     * checkToken
     *
     * @param token
     * @return
     */
    public Map<String, Object> checkToken(String token) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add(TOKEN, token);

            HttpEntity httpEntity = new HttpEntity(params, headers);

            ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(AUTH_SERVICE + "/oauth/check_token", POST, httpEntity, new ParameterizedTypeReference<Map<String, Object>>() {
            });
            return responseEntity.getBody();
        } catch (Exception e) {
            log.error("checkToken error - {}, {}", ExceptionUtils.getStackTrace(e), token);
            return null;
        }
    }

    /**
     * refreshToken
     *
     * @param refreshToken
     * @return
     */
    public OAuth2AccessToken refreshToken(String refreshToken) {
        try {
            ClientInfo clientInfo = Client.info();
            String clientID = clientInfo.getClientID();
            String clientSecret = clientInfo.getClientSecret();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add(CLIENT_ID, clientID);
            params.add(CLIENT_SECRET, clientSecret);
            params.add(REFRESH_TOKEN, refreshToken);
            params.add(GRANT_TYPE, GrantType.REFRESH_TOKEN.getValue());

            HttpEntity httpEntity = new HttpEntity(params, headers);

            ResponseEntity<OAuth2AccessToken> responseEntity = restTemplate.exchange(AUTH_SERVICE + "/oauth/token", POST, httpEntity, OAuth2AccessToken.class);
            return responseEntity.getBody();
        } catch (Exception e) {
            log.error("refreshToken error - {}, {}", ExceptionUtils.getStackTrace(e), refreshToken);
        }
        return null;
    }
}
