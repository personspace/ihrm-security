package cn.ambow.tokenhandler;

import cn.ambow.pojo.JwtToken;
import cn.ambow.pojo.bo.ClientInfo;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

/**
 * 访问令牌接口服务
 *
 * @author shanglei
 * @date 2020/12/23
 */
public interface AccessTokenService {
    JwtToken addToken(ClientInfo info, String username, OAuth2AccessToken token);

    JwtToken refreshToken(ClientInfo info, String username, OAuth2AccessToken token);

    void deleteToken(ClientInfo info, String username);
}
