package cn.ambow.tokenhandler.impl;


import cn.ambow.pojo.SimpleJwtToken;
import cn.ambow.pojo.LoginUser;
import cn.ambow.pojo.po.Token;
import cn.ambow.repository.TokenRepository;
import cn.ambow.serialization.ObjectToJsonService;
import cn.ambow.tokenhandler.TraditionalTokenService;
import cn.ambow.util.DateTimeUtil;
import cn.ambow.util.JwtTokenUtil;
import cn.ambow.util.SecurityUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * token存到数据库的实现类
 */
@Primary
@Service
public class TraditionalTokenServiceDbImpl implements TraditionalTokenService {

    /**
     * token过期秒数
     */
    @Value("${token.expire.seconds}")
    private Integer expireSeconds;

    @Resource
    private ObjectToJsonService objectToJsonService;

    @Resource
    private TokenRepository tokenRepository;
    /**
     * 私钥
     */
    @Value("${token.jwtSecret}")
    private String jwtSecret;

    @Transactional
    @Override
    public SimpleJwtToken saveToken(LoginUser loginUser) {
        loginUser.setTokenID(UUID.randomUUID().toString());
        loginUser.setLoginTime(System.currentTimeMillis());
        loginUser.setExpireTime(loginUser.getLoginTime() + expireSeconds * 1000);

        Token token = new Token();
        token.setId(loginUser.getTokenID());
        token.setCreateTime(LocalDateTime.now());
        token.setModifyTime(LocalDateTime.now());
        token.setExpireTime(DateTimeUtil.parse(loginUser.getExpireTime()));
        token.setUser(objectToJsonService.toJson(loginUser));
        tokenRepository.insert(token);
        String jwtToken = JwtTokenUtil.getJwtToken(loginUser, jwtSecret);
        return new SimpleJwtToken(jwtToken, DateTimeUtil.parse(loginUser.getLoginTime()), DateTimeUtil.parse(loginUser.getExpireTime()));
    }

    @Transactional
    @Override
    public void refresh(LoginUser loginUser) {
        loginUser.setLoginTime(System.currentTimeMillis());
        loginUser.setExpireTime(loginUser.getLoginTime() + expireSeconds * 1000);

        Token token = tokenRepository.select(loginUser.getTokenID());
        token.setModifyTime(LocalDateTime.now());
        token.setExpireTime(DateTimeUtil.parse(loginUser.getExpireTime()));
        token.setUser(objectToJsonService.toJson(loginUser));
        tokenRepository.update(token);
    }

    @Override
    public LoginUser getLoginUser(String jwtToken) {
        String tokenID = JwtTokenUtil.getTokenID(jwtToken, jwtSecret);
        if (StringUtils.isNotEmpty(tokenID)) {
            Token token = tokenRepository.select(tokenID);
            return getLoginUser(token);
        }
        return null;
    }

    @Override
    public int deleteToken(String jwtToken) {
        String tokenID = JwtTokenUtil.getTokenID(jwtToken, jwtSecret);
        if (StringUtils.isNotEmpty(tokenID)) {
            Token token = tokenRepository.select(tokenID);
            LoginUser loginUser = getLoginUser(token);
            if (loginUser != null) {
                tokenRepository.delete(tokenID);
                SecurityUtil.remove();
                return 1;
            }
        }
        return 0;
    }

    /**
     * 获取登录用户
     *
     * @param token
     * @return
     */
    private LoginUser getLoginUser(Token token) {
        if (token == null) {
            return null;
        }
        // 校验是否已过期
        if (DateTimeUtil.toTimestamp(token.getExpireTime()) > System.currentTimeMillis()) {
            return objectToJsonService.toObject(token.getUser(), LoginUser.class);
        }
        return null;
    }


}
