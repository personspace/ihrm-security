package cn.ambow.tokenhandler.impl;

import cn.ambow.pojo.JwtToken;
import cn.ambow.pojo.bo.ClientInfo;
import cn.ambow.tokenhandler.AccessTokenService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Set;
import java.util.UUID;

import static cn.ambow.constant.RedisKeys.*;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * 令牌服务.
 *
 * @author Peter
 */
@Slf4j
@Service
public class RedisAccessTokenServiceImpl implements AccessTokenService {

    private final static String USER_NAME_FORMAT = "%s_%s_%s";

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 添加令牌
     *
     * @param info
     * @param username
     * @param token
     * @return
     */
    public JwtToken addToken(ClientInfo info, String username, OAuth2AccessToken token) {
        try {
            if (token != null) {
                String clientID = info.getClientID();
                String clientType = info.getClientType();
                String tokenType = token.getTokenType();
                int expiresIn = token.getExpiresIn() - 10;

                String accessToken = UUID.randomUUID().toString();
                String refreshToken = UUID.randomUUID().toString();
                String accessTokenWithType = String.format("%s %s", tokenType, accessToken);
                String accessTokenKey = String.format(ACCESS_TOKEN_KEY_FORMAT, accessTokenWithType);
                String refreshTokenKey = String.format(REFRESH_TOKEN_KEY_FORMAT, refreshToken);
                String accessTokenValue = token.getValue();
                String refreshTokenValue = token.getRefreshToken().getValue();
                stringRedisTemplate.opsForValue().set(accessTokenKey, accessTokenValue, expiresIn, SECONDS);
                stringRedisTemplate.opsForValue().set(refreshTokenKey, refreshTokenValue, expiresIn, SECONDS);

                username = String.format(USER_NAME_FORMAT, clientType, clientID, username);
                String userTokenKey = String.format(USER_TOKEN_KEY_FORMAT, username);
                if (stringRedisTemplate.hasKey(userTokenKey)) {
                    Set<String> set = stringRedisTemplate.opsForSet().members(userTokenKey);
                    stringRedisTemplate.delete(set);
                    stringRedisTemplate.delete(userTokenKey);
                }
                stringRedisTemplate.opsForSet().add(userTokenKey, accessTokenKey);
                stringRedisTemplate.expire(userTokenKey, expiresIn, SECONDS);

                JwtToken jwtToken = new JwtToken();
                jwtToken.setAccessToken(accessToken);
                jwtToken.setTokenType(tokenType);
                jwtToken.setRefreshToken(refreshToken);
                jwtToken.setExpiresIn(token.getExpiresIn());
                return jwtToken;
            }
        } catch (Exception e) {
            log.error("addToken error - {}, {}, {}, {}", ExceptionUtils.getStackTrace(e), info, username, token);
        }
        return null;
    }

    /**
     * 刷新令牌
     *
     * @param info
     * @param username
     * @param token
     * @return
     */
    public JwtToken refreshToken(ClientInfo info, String username, OAuth2AccessToken token) {
        try {
            if (token != null) {
                String clientID = info.getClientID();
                String clientType = info.getClientType();
                String tokenType = token.getTokenType();
                int expiresIn = token.getExpiresIn() - 10;

                String accessToken = UUID.randomUUID().toString();
                String refreshToken = UUID.randomUUID().toString();
                String accessTokenWithType = String.format("%s %s", tokenType, accessToken);
                String accessTokenKey = String.format(ACCESS_TOKEN_KEY_FORMAT, accessTokenWithType);
                String refreshTokenKey = String.format(REFRESH_TOKEN_KEY_FORMAT, refreshToken);
                String accessTokenValue = token.getValue();
                String refreshTokenValue = token.getRefreshToken().getValue();
                stringRedisTemplate.opsForValue().set(accessTokenKey, accessTokenValue, expiresIn, SECONDS);
                stringRedisTemplate.opsForValue().set(refreshTokenKey, refreshTokenValue, expiresIn, SECONDS);

                username = String.format(USER_NAME_FORMAT, clientType, clientID, username);
                String userTokenKey = String.format(USER_TOKEN_KEY_FORMAT, username);
                stringRedisTemplate.opsForSet().add(userTokenKey, accessTokenKey);
                stringRedisTemplate.expire(userTokenKey, expiresIn, SECONDS);

                JwtToken jwtToken = new JwtToken();
                jwtToken.setAccessToken(accessToken);
                jwtToken.setTokenType(tokenType);
                jwtToken.setRefreshToken(refreshToken);
                jwtToken.setExpiresIn(token.getExpiresIn());
                return jwtToken;
            }
        } catch (Exception e) {
            log.error("refreshToken error - {}, {}, {}, {}", ExceptionUtils.getStackTrace(e), info, username, token);
        }
        return null;
    }

    /**
     * 删除令牌
     *
     * @param info
     * @param username
     */
    public void deleteToken(ClientInfo info, String username) {
        try {
            String clientID = info.getClientID();
            String clientType = info.getClientType();
            username = String.format(USER_NAME_FORMAT, clientType, clientID, username);
            String userTokenKey = String.format(USER_TOKEN_KEY_FORMAT, username);
            if (stringRedisTemplate.hasKey(userTokenKey)) {
                Set<String> set = stringRedisTemplate.opsForSet().members(userTokenKey);
                stringRedisTemplate.delete(set);
                stringRedisTemplate.delete(userTokenKey);
            }
        } catch (Exception e) {
            log.error("deleteToken error - {}, {}, {}", ExceptionUtils.getStackTrace(e), info, username);
        }
    }
}
