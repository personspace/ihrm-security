package cn.ambow.tokenhandler.impl;

import cn.ambow.pojo.JwtToken;
import cn.ambow.pojo.bo.ClientInfo;
import cn.ambow.pojo.po.Token;
import cn.ambow.repository.TokenRepository;
import cn.ambow.tokenhandler.AccessTokenService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * 令牌服务.
 *
 * @author Peter
 */
@Slf4j
@Service
public class DBAccessTokenServiceImpl implements AccessTokenService {

    private final static String USER_NAME_FORMAT = "%s_%s_%s";

    @Resource
    private TokenRepository tokenRepository;

    /**
     * 添加令牌
     *
     * @param info
     * @param username
     * @param token
     * @return
     */
    public JwtToken addToken(ClientInfo info, String username, OAuth2AccessToken token) {
        try {
            if (token != null) {
                String clientID = info.getClientID();
                String clientType = info.getClientType();
                String tokenType = token.getTokenType();
                int expiresIn = token.getExpiresIn() - 10;

                String accessTokenID = UUID.randomUUID().toString();
                String refreshTokenID = UUID.randomUUID().toString();
                String accessTokenValue = token.getValue();
                String refreshTokenValue = token.getRefreshToken().getValue();

                String tokenID = String.format(USER_NAME_FORMAT, clientID, clientType, username);
                LocalDateTime now = LocalDateTime.now();
                Token dbToken = tokenRepository.select(tokenID);
                if (dbToken != null) {
                    dbToken.setAccessTokenID(accessTokenID);
                    dbToken.setAccessToken(accessTokenValue);
                    dbToken.setRefreshTokenID(refreshTokenID);
                    dbToken.setRefreshToken(refreshTokenValue);
                    dbToken.setModifyTime(now);
                    dbToken.setExpireTime(now.plusSeconds(expiresIn));
                    tokenRepository.update(dbToken);
                } else {
                    dbToken = new Token();
                    dbToken.setId(tokenID);
                    dbToken.setAccessTokenID(accessTokenID);
                    dbToken.setAccessToken(accessTokenValue);
                    dbToken.setRefreshTokenID(refreshTokenID);
                    dbToken.setRefreshToken(refreshTokenValue);
                    dbToken.setCreateTime(now);
                    dbToken.setExpireTime(now.plusSeconds(expiresIn));
                    tokenRepository.insert(dbToken);
                }

                JwtToken jwtToken = new JwtToken();
                jwtToken.setAccessToken(accessTokenID);
                jwtToken.setTokenType(tokenType);
                jwtToken.setRefreshToken(refreshTokenID);
                jwtToken.setExpiresIn(token.getExpiresIn());
                return jwtToken;
            }
        } catch (Exception e) {
            log.error("addToken error - {}, {}, {}, {}", ExceptionUtils.getStackTrace(e), info, username, token);
        }
        return null;
    }

    /**
     * 刷新令牌
     *
     * @param info
     * @param username
     * @param token
     * @return
     */
    public JwtToken refreshToken(ClientInfo info, String username, OAuth2AccessToken token) {
        try {
            if (token != null) {
                String clientID = info.getClientID();
                String clientType = info.getClientType();
                String accessTokenID = UUID.randomUUID().toString();
                String refreshTokenID = UUID.randomUUID().toString();
                String accessTokenValue = token.getValue();
                String refreshTokenValue = token.getRefreshToken().getValue();
                String tokenType = token.getTokenType();
                int expiresIn = token.getExpiresIn();

                String tokenID = String.format(USER_NAME_FORMAT, clientID, clientType, username);
                LocalDateTime now = LocalDateTime.now();
                Token dbToken = tokenRepository.select(tokenID);
                if (dbToken != null) {
                    dbToken.setAccessTokenID(accessTokenID);
                    dbToken.setAccessToken(accessTokenValue);
                    dbToken.setRefreshTokenID(refreshTokenID);
                    dbToken.setRefreshToken(refreshTokenValue);
                    dbToken.setCreateTime(now);
                    dbToken.setExpireTime(now.plusSeconds(expiresIn));
                    tokenRepository.update(dbToken);
                }
                JwtToken jwtToken = new JwtToken();
                jwtToken.setAccessToken(accessTokenID);
                jwtToken.setTokenType(tokenType);
                jwtToken.setRefreshToken(refreshTokenID);
                jwtToken.setExpiresIn(token.getExpiresIn());
                return jwtToken;
            }
        } catch (Exception e) {
            log.error("refreshToken error - {}, {}, {}, {}", ExceptionUtils.getStackTrace(e), info, username, token);
        }
        return null;
    }

    /**
     * 删除令牌
     *
     * @param info
     * @param username
     */
    public void deleteToken(ClientInfo info, String username) {
        try {
            String clientID = info.getClientID();
            String clientType = info.getClientType();
            String tokenID = String.format(USER_NAME_FORMAT, clientID, clientType, username);
            tokenRepository.delete(tokenID);
        } catch (Exception e) {
            log.error("deleteToken error - {}, {}, {}", ExceptionUtils.getStackTrace(e), info, username);
        }
    }
}
