package cn.ambow.tokenhandler.impl;


import cn.ambow.pojo.SimpleJwtToken;
import cn.ambow.pojo.LoginUser;
import cn.ambow.serialization.ObjectToJsonService;
import cn.ambow.tokenhandler.TraditionalTokenService;
import cn.ambow.util.DateTimeUtil;
import cn.ambow.util.SecurityUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * token存到redis
 */
@Service
public class TraditionalTokenServiceRedisImpl implements TraditionalTokenService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * token过期秒数
     */
    @Value("${token.expire.seconds}")
    private Integer expireSeconds;

    @Resource
    private ObjectToJsonService objectToJsonService;

    /**
     * 私钥
     */
    @Value("${token.jwtSecret}")
    private String jwtSecret;

    private static final String TOKEN_KEY = "token_%s";

    @Override
    public SimpleJwtToken saveToken(LoginUser loginUser) {
        loginUser.setTokenID(UUID.randomUUID().toString());
        loginUser.setLoginTime(System.currentTimeMillis());
        loginUser.setExpireTime(loginUser.getLoginTime() + expireSeconds * 1000);
        //放入Redis
        stringRedisTemplate.opsForValue().set(String.format(TOKEN_KEY, loginUser.getTokenID()), objectToJsonService.toJson(loginUser));
        return new SimpleJwtToken(loginUser.getTokenID(), DateTimeUtil.parse(loginUser.getLoginTime()), DateTimeUtil.parse(loginUser.getExpireTime()));
    }

    @Override
    public void refresh(LoginUser loginUser) {
        Boolean isDeleted = stringRedisTemplate.delete(String.format(TOKEN_KEY, loginUser.getTokenID()));
        if (isDeleted) {
            loginUser.setLoginTime(System.currentTimeMillis());
            loginUser.setExpireTime(loginUser.getLoginTime() + expireSeconds * 1000);
            //放入Redis
            stringRedisTemplate.opsForValue().set(String.format(TOKEN_KEY, loginUser.getTokenID()), objectToJsonService.toJson(loginUser));
        }
    }

    @Override
    public LoginUser getLoginUser(String token) {
        String user = stringRedisTemplate.opsForValue().get(String.format(TOKEN_KEY, token));
        LoginUser loginUser = StringUtils.isNotEmpty(user) ? objectToJsonService.toObject(user, LoginUser.class) : null;
        return loginUser;
    }

    @Override
    public int deleteToken(String token) {
        Boolean isDeleted = stringRedisTemplate.delete(String.format(TOKEN_KEY, token));
        if (isDeleted) {
            SecurityUtil.remove();
            return 1;
        }
        return 0;
    }
}
