package cn.ambow.exception;

import cn.ambow.response.HttpRes;
import cn.ambow.response.ResponseResult;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.*;

/**
 * 全局异常处理
 *
 * @author shanglei
 * @date 2020/11/28
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ResponseResult handleException(Exception e) {
        return HttpRes.error(ExceptionUtils.getMessage(e));
    }

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ExceptionHandler(SystemException.class)
    public ResponseResult handleSystemException(SystemException e) {
        return HttpRes.error(e.getCode(), e.getMessage());
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(ServiceException.class)
    public ResponseResult handleServiceException(ServiceException e) {
        return HttpRes.error(e.getCode(), e.getMessage());
    }

    @ResponseStatus(FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseResult handleAccessDeniedException(AccessDeniedException e) {
        return HttpRes.error(FORBIDDEN.value(), "没有权限");
    }

}
