package cn.ambow.pojo.dto;

import lombok.Data;

/**
 * @author shanglei
 * @date 2020/12/24
 */
@Data
public class TokenReq {
    private String token;
}
