package cn.ambow.oauth2;

import org.springframework.security.oauth2.provider.authentication.TokenExtractor;

import javax.servlet.http.HttpServletRequest;

/**
 * 令牌提取服务接口
 *
 * @author shanglei
 * @date 2020/12/23
 */
public interface TokenExtractorService extends TokenExtractor {

    String extractToken(HttpServletRequest request);
}
