package cn.ambow.util;

import cn.ambow.pojo.LoginUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

/**
 * 鉴权工具类
 *
 * @author shanglei
 * @date 2020/12/8
 */
public class SecurityUtil {
    /**
     * auth
     *
     * @return
     */
    public static OAuth2Authentication auth() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth instanceof OAuth2Authentication ? (OAuth2Authentication) auth : null;
    }

    /**
     * user
     *
     * @return
     */
    public static User user() {
        OAuth2Authentication auth = auth();
        if (auth.getPrincipal() instanceof User) {
            return (User) auth.getPrincipal();
        }
        return null;
    }

    /**
     * getUsername
     *
     * @return
     */
    public static String getUsername() {
        OAuth2Authentication auth = auth();
        return auth != null ? auth.getName() : null;
    }
    /**
     * 退出时清空上下文中的Authentication对象
     */
    public static void remove() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }
}
