package cn.ambow.util;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 时间工具类.
 *
 * @author Peter
 */
public class DateTimeUtil {

    public static final ZoneOffset OFFSET = ZoneOffset.of("+08:00");
    public static final ZoneId ZONE = ZoneId.of("GMT+8");
    public static final String DATE_TIME_FORMATTER = "yyyy-MM-dd HH:mm:ss";
    public static final String LOG_TIME_FORMATTER = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String DATE_FORMATTER = "yyyy-MM-dd";
    public static final String MONTH_FORMATTER = "yyyy-MM";
    public static final String TIME_FORMATTER = "HH:mm:ss";
    public static final DateTimeFormatter NGINX_TIME_FORMATTER;

    static {
        Map<Long, String> moy = new HashMap<>();
        moy.put(1L, "Jan");
        moy.put(2L, "Feb");
        moy.put(3L, "Mar");
        moy.put(4L, "Apr");
        moy.put(5L, "May");
        moy.put(6L, "Jun");
        moy.put(7L, "Jul");
        moy.put(8L, "Aug");
        moy.put(9L, "Sep");
        moy.put(10L, "Oct");
        moy.put(11L, "Nov");
        moy.put(12L, "Dec");
        NGINX_TIME_FORMATTER = new DateTimeFormatterBuilder()
                .appendValue(ChronoField.DAY_OF_MONTH, 2)
                .appendLiteral("/")
                .appendText(ChronoField.MONTH_OF_YEAR, moy)
                .appendLiteral("/")
                .appendValue(ChronoField.YEAR_OF_ERA, 4, 19, SignStyle.EXCEEDS_PAD)
                .appendLiteral(":")
                .append(DateTimeFormatter.ISO_LOCAL_TIME)
                .appendLiteral(" ")
                .appendOffset("+HHMM", "+0000")
                .toFormatter();
    }

    /**
     * betweenDate
     *
     * @param fromDt
     * @param toDt
     * @return
     */
    public static List<String> betweenDate(LocalDate fromDt, LocalDate toDt) {
        List<String> dateList = new ArrayList<>();
        LocalDate date = fromDt;
        LocalDate maxDt = toDt;
        while (!date.isAfter(maxDt)) {
            dateList.add(date.toString());
            date = date.plusDays(1);
        }
        return dateList;
    }

    /**
     * format
     *
     * @param datetime
     * @return
     */
    public static String format(LocalDateTime datetime) {
        if (datetime != null) {
            return format(datetime, DATE_TIME_FORMATTER);
        }
        return "";
    }

    /**
     * format
     *
     * @param datetime
     * @param formatter
     * @return
     */
    public static String format(LocalDateTime datetime, String formatter) {
        return datetime.format(DateTimeFormatter.ofPattern(formatter));
    }

    /**
     * parse
     *
     * @param datetime
     * @return
     */
    public static LocalDateTime parse(String datetime) {
        return parse(datetime, DATE_TIME_FORMATTER);
    }

    /**
     * parse
     *
     * @param datetime
     * @param formatter
     * @return
     */
    public static LocalDateTime parse(String datetime, String formatter) {
        return LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern(formatter));
    }

    /**
     * parse
     *
     * @param dateTime
     * @param formatter
     * @return
     */
    public static LocalDateTime parse(String dateTime, DateTimeFormatter formatter) {
        return LocalDateTime.parse(dateTime, formatter);
    }

    /**
     * 时间戳转LocalDateTime
     *
     * @param timestamp
     * @return
     */
    public static LocalDateTime parse(long timestamp) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZONE);
    }

    /**
     * LocalDateTime转毫秒数
     */
    public static long toTimestamp(LocalDateTime localDateTime) {
        return localDateTime.toInstant(OFFSET).toEpochMilli();
    }
}
