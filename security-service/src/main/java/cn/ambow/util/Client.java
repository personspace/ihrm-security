package cn.ambow.util;

import cn.ambow.pojo.bo.ClientInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static cn.ambow.pojo.bo.Headers.CLIENT;

/**
 * 应用
 *
 * @author Peter
 */
@Slf4j
public class Client {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * info
     *
     * @return
     */
    public static ClientInfo info() {
        try {
            return info(header());
        } catch (Exception e) {
            log.error("info error - {}", ExceptionUtils.getMessage(e));
        }
        return defaultInfo();
    }

    /**
     * info
     *
     * @return
     */
    public static ClientInfo info(String header) {
        try {
            if (StringUtils.isNotEmpty(header)) {
                byte[] json = Base64Utils.decodeFromString(header);
                return objectMapper.readValue(json, ClientInfo.class);
            }
        } catch (Exception e) {
            log.error("info error - {}", ExceptionUtils.getMessage(e));
        }
        return defaultInfo();
    }

    /**
     * clientID
     *
     * @return
     */
    public static String clientID() {
        return info().getClientID();
    }

    /**
     * clientType
     *
     * @return
     */
    public static String clientType() {
        return info().getClientType();
    }

    /**
     * header
     *
     * @return
     */
    public static String header() {
        try {
            RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
            if (requestAttributes != null) {
                HttpServletRequest servletRequest = ((ServletRequestAttributes) requestAttributes).getRequest();
                return servletRequest.getHeader(CLIENT);
            }
        } catch (Exception e) {
            log.error("header error - {}", ExceptionUtils.getMessage(e));
        }
        return null;
    }

    /**
     * token
     *
     * @return
     */
    public static String token() {
        try {
            RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
            if (requestAttributes != null) {
                HttpServletRequest servletRequest = ((ServletRequestAttributes) requestAttributes).getRequest();
                return servletRequest.getHeader("Authorization");
            }
        } catch (Exception e) {
            log.error("token error - {}", ExceptionUtils.getMessage(e));
        }
        return null;
    }

    /**
     * header
     *
     * @param clientID
     * @param clientType
     * @return
     */
    public static String header(String clientID, String clientType) {
        try {
            ClientInfo info = new ClientInfo();
            info.setClientID(clientID);
            info.setClientType(clientType);
            String json = objectMapper.writeValueAsString(info);
            return Base64Utils.encodeToString(json.getBytes());
        } catch (Exception e) {
            log.error("header error - {}, {}", ExceptionUtils.getMessage(e), clientID);
        }
        return null;
    }

    /**
     * 测试客户端信息
     *
     * @return
     */
    private static ClientInfo defaultInfo() {
        return new ClientInfo();
    }

//    public static void main(String[] args) throws JsonProcessingException {
//        ClientInfo clientInfo = new ClientInfo();
//        clientInfo.setClientID("test");
//        clientInfo.setClientSecret("test");
//        clientInfo.setClientType("admin");
//        String json = objectMapper.writeValueAsString(clientInfo);
//        System.out.println(json);
//        System.out.println(Base64Utils.encodeToString(json.getBytes()));
//
//        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//        System.out.println(passwordEncoder.encode("test"));
//        System.out.println(passwordEncoder.encode("test"));
//    }

//    public static void main(String[] args) throws JsonProcessingException {
//        ClientInfo clientInfo = new ClientInfo();
//        clientInfo.setClientID("test");
//        clientInfo.setClientSecret("test");
//        clientInfo.setClientType(ClientType.APP.name());
//        String json = objectMapper.writeValueAsString(clientInfo);
//        String requestClient = Base64Utils.encodeToString(json.getBytes());
//        System.out.println(CLIENT);
//        System.out.println(requestClient);
//    }
}
