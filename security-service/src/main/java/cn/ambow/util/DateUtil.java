package cn.ambow.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * 日期工具类.
 *
 * @author Peter
 */
public class DateUtil {
    public static final String SIMPLE_DATE_FORMATTER = "yyyyMMdd";
    public static final String DATE_FORMATTER = "yyyy-MM-dd";

    /**
     * parse
     *
     * @param date
     * @return
     */
    public static LocalDate parse(String date) {
        return parse(date, "yyyy-MM-dd");
    }

    /**
     * parse
     *
     * @param date
     * @param formatter
     * @return
     */
    public static LocalDate parse(String date, String formatter) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(formatter));
    }
}
