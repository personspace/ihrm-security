package cn.ambow.util;

import cn.ambow.pojo.LoginUser;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * jwtToken工具类
 *
 * @author shanglei
 * @date 2020/12/14
 */
@Slf4j
public class JwtTokenUtil {

    public static final String TOKEN_KEY = "Authorization";

    /**
     * 从header获取token
     *
     * @param request
     * @return
     */
    public static String getJwtToken(HttpServletRequest request) {
        String token = request.getHeader(TOKEN_KEY);
        if (StringUtils.isNotEmpty(token)) {
            token = token.replace("Bearer ", "");
        }
        return token;
    }


    /**
     * 获取secretKey
     *
     * @return
     */
    private static SecretKey getKeyInstance(String jwtSecret) throws Exception {
        byte[] encode = Base64.getEncoder().encode(jwtSecret.getBytes(StandardCharsets.UTF_8));
        SecretKey secretKey = new SecretKeySpec(encode, SignatureAlgorithm.HS256.getJcaName());
        return secretKey;
    }

    /**
     * 生成jwtToken
     *
     * @param loginUser
     * @return
     */
    public static String getJwtToken(LoginUser loginUser, String jwtSecret) {
        Map<String, Object> claims = new HashMap<>();
        try {
            claims.put("token_id", loginUser.getTokenID()); //放一个tokenID,用来去数据库反向得到对应的user的json串
            claims.put("expireTime", loginUser.getExpireTime());
            String jwtToken = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS256, getKeyInstance(jwtSecret))
                    .compact();

            return jwtToken;
        } catch (Exception e) {
            log.error("base64加密出错------{}", ExceptionUtils.getStackTrace(e));
        }
        return null;
    }

    /**
     * 根据token获取数据库tokenID
     *
     * @param token
     * @return
     */
    public static String getTokenID(String token, String jwtSecret) {
        if (StringUtils.isEmpty(token)) {
            return null;
        }
        Map<String, Object> jwtClaims = null;
        try {
            jwtClaims = Jwts.parser().setSigningKey(getKeyInstance(jwtSecret)).parseClaimsJws(token).getBody();
            return (String) jwtClaims.get("token_id");
        } catch (ExpiredJwtException e) {
            log.error("token已过期------{}", token);
        } catch (Exception e) {
            log.error("token解密出错------{}", ExceptionUtils.getStackTrace(e));
        }
        return null;
    }
}
