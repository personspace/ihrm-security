//package cn.ambow.filter;
//
//
//import cn.ambow.pojo.LoginUser;
//import cn.ambow.tokenhandler.TokenService;
//import cn.ambow.util.JwtTokenUtil;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import javax.annotation.Resource;
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// * Token过滤器
// *
// * @author 小威老师 xiaoweijiagou@163.com
// * <p>
// * 2017年10月14日
// */
//@Component
//public class TokenAuthFilter extends OncePerRequestFilter {
//
//    @Resource
//    private TokenService tokenService;
//
//    @Resource
//    private UserDetailsService userDetailsService;
//
//    private static final Long MINUTES_10 = 10 * 60 * 1000L;
//
//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
//            throws ServletException, IOException {
//        String token = JwtTokenUtil.getJwtToken(request);
//        if (StringUtils.isNotBlank(token)) {
//            LoginUser loginUser = tokenService.getLoginUser(token);
//            if (loginUser != null) {
//                loginUser = checkLoginTime(loginUser);
//                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(loginUser,
//                        null, loginUser.getAuthorities());
//                SecurityContextHolder.getContext().setAuthentication(authentication);
//
//            }
//        }
//
//        filterChain.doFilter(request, response);
//    }
//
//    /**
//     * 校验时间<br>
//     * 过期时间与当前时间对比，临近过期10分钟内的话，自动刷新缓存
//     *
//     * @param loginUser
//     * @return
//     */
//    private LoginUser checkLoginTime(LoginUser loginUser) {
//        long expireTime = loginUser.getExpireTime();
//        long currentTime = System.currentTimeMillis();
//        if (expireTime - currentTime <= MINUTES_10) {
//            String token = loginUser.getTokenID();
//            loginUser = (LoginUser) userDetailsService.loadUserByUsername(loginUser.getUsername());
//            loginUser.setTokenID(token);
//            tokenService.refresh(loginUser);
//        }
//        return loginUser;
//    }
//
//}
