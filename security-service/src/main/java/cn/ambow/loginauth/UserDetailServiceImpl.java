package cn.ambow.loginauth;

import cn.ambow.pojo.LoginUser;
import cn.ambow.pojo.po.Permission;
import cn.ambow.pojo.po.User;
import cn.ambow.repository.PermissionRepository;
import cn.ambow.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户认证实现
 *
 * @author shanglei
 * @date 2020/12/7
 */
@Service
@Slf4j
public class UserDetailServiceImpl implements UserDetailsService {

    @Resource
    private UserRepository userRepository;

    @Resource
    private PermissionRepository permissionRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.selectByName(username);
        if (user != null) {
            //查询用户所有权限
            List<Permission> permissions = permissionRepository.selectListByUserID(user.getUserID());
            LoginUser loginUser = new LoginUser();
            BeanUtils.copyProperties(user, loginUser);
            loginUser.setPermissions(permissions);
            return loginUser;
        }
        return null;
    }
}
